GENERAL
=========
+ Multiple if statements will evaluate each one whereas if-else-if will skip the else or the if.
POINTERS
=========
+ int *x -> x is a pointer to an integer location/address or x points to an integer location/address.
+ x++ is incrementing a pointer it'll increment to the next address in memory.
+ if name[] is an array, if j = name, j holds the base address of the array. (Check to see how the data type of j influences the array; e.g if name[] is an integer array, j should be an integer pointer.)