#include <stdio.h>
int swapr(int *x, int *y);
int main(void)
{
  int value1 = 0, value2 = 0;
  printf("Please enter two values separated by a comma: ");
  scanf("%d,%d", &value1, &value2);//values 1 and 2 are in scanf
  swapr(&value1, &value2);// swapr takes in addresses of 1 and 2
  printf("Value 1 is now: %d\n Value 2 is now: %d\n", value1, value2);
  return 0;
}

int swapr(int *x, int *y) //x is a pointer to an integer location; x points to an integer address; x holds an integer address; 
{
  int t;
  t = *x; // t now holds the value at address x
  *x = *y; //value at address x is swapped with value at address y
  *y = t; //value at address y is swapped with value at address x 
 return 0;
}
