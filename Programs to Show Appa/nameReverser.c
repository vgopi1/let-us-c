/*
 * nameReverser.c
 *  Purpose: Reverse a name entered as a string
 *  Created on: Jul 13, 2020
 *      Author: Vishnuu
*/
#include <stdio.h>
int nameReverser();
int main(void)
{

	printf("I am in a.c\n");
	nameReverser();
	return 0; 
}

int nameReverser()
{
    char name[25];
    printf("Enter a first name:");
    scanf("%s", name);
    for(int i = 25; i >= 0; i--)
    {
        printf("%c", name[i]);
    
    }
    printf("\n");
    return 0;
}
