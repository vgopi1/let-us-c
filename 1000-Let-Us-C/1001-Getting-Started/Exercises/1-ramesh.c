/*
 * @Author: Vishnuu Gopi 
 * @Date: 2020-08-09 00:19:40 
 * @Last Modified by: Vishnuu Gopi
 * @Last Modified time: 2020-08-09 00:26:40
 * @Prompt: Ramesh’s basic salary is input through the keyboard. His dearness allowance is 40% of basic salary, and * rent allowance is 20% of basic salary. Write a program to calculate his gross salary.
*/

#include <stdio.h>
int main(void)
{
    float baseSalary = 0;
    
    printf("Please enter the base salary\n");
    scanf("%f", &baseSalary);
    printf("Ramesh's gross salary is: $%.2f\n", 0.4*baseSalary + 0.2*baseSalary + baseSalary);
}