/*
 * @Author: vgopi1 
 * @Date: 2020-08-09 01:20:06 
 * @Last Modified by: vgopi1
 * @Last Modified time: 2020-08-09 01:37:39
 * @Prompt: Paper of size A0 has dimensions 1189 mm x 841 mm. Each
 * subsequent size A(n) is defined as A(n-1) cut in half parallel to its
 * shorter sides. Thus paper of size A1 would have dimensions 841
mm x 594 mm. Write a program to calculate and print paper sizes
A0, A1, A2, ... A8.
 */

#include <stdio.h>

int main(void)
{
    //A0 -> 1189mm * 841mm
    //A1 = 
    int a0l = 1189, a0w = 841; //@var a0l = A0 Length, a0w = AO Width

    printf("Paper A0 dimensions: %dmm x %dmm\n", a0l, a0w);
    int a1w = a0l/2;
    int a1l = a0w; 
    printf("Paper A1 dimensions: %dmm x %dmm\n", a1l, a1w);
    int a2w = a1l/2; int a2l = a1w;
    printf("Paper A2 dimensions: %dmm x %dmm\n", a2l, a2w);
    int a3w = a2l/2; int a3l = a2w;
    printf("Paper A3 dimensions: %dmm x %dmm\n", a3l, a3w);
    int a4w = a3l/2; int a4l = a3w;
    printf("Paper A4 dimensions: %dmm x %dmm\n", a4l, a4w);
    int a5w = a4l/2; int a5l = a4w;
    printf("Paper A5 dimensions: %dmm x %dmm\n", a5l, a5w);
    int a6w = a5l/2; int a6l = a5w;
    printf("Paper A6 dimensions: %dmm x %dmm\n", a6l, a6w);
    int a7w = a6l/2; int a7l = a6w; 
    printf("Paper A7 dimensions: %dmm x %dmm\n", a7l, a7w);
    int a8w = a7l/2; int a8l = a7w;
    printf("Paper A8 dimensions: %dmm x %dmm\n", a8l, a8w);
    
    return 0;
}
