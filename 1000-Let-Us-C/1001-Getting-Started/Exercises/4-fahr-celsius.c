/*
 * @Author: vgopi1 
 * @Date: 2020-08-09 00:58:40 
 * @Last Modified by: vgopi1
 * @Last Modified time: 2020-08-09 01:05:44
 */
/* Temperature of a city in Fahrenheit degrees is input through the
keyboard. Write a program to convert this temperature into
Centigrade degrees. */
#include <stdio.h>
int main(void)
{
    float fahrTemp = 0, celsiusTemp = 0;
    
    printf("Enter a fahrenheit temperature: ");
    scanf("%f", &fahrTemp);
    celsiusTemp = (fahrTemp-32) * 5 / 9;
    printf("The temperature is %.2f\n degrees celsius", celsiusTemp);

    return 0;
}