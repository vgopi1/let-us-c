/*
 * @Author: vgopi1 
 * @Date: 2020-08-09 00:47:49 
 * @Last Modified by: vgopi1
 * @Last Modified time: 2020-08-09 00:56:21
 */

/* If the marks obtained by a student in five different subjects are
input through the keyboard, write a program to find out the
aggregate marks and percentage marks obtained by the student.
Assume that the maximum marks that can be obtained by a student
in each subject is 100. */

#include <stdio.h>
int main(void)
{
    float mark1 = 0, mark2 = 0, mark3 = 0, mark4 = 0, mark5 = 0;
    printf("Enter the marks obtained by a student in five different subjects:\n");
    scanf("%f %f %f %f %f", &mark1, &mark2, &mark3, &mark4, &mark5);
    printf("The aggregate marks are: %.2f\n", mark1 + mark2 + mark3 + mark4 + mark5);
    printf("The percentage marks are: %.2f\n", (mark1 + mark2 + mark3 + mark4 + mark5)/5);

    return 0;
}