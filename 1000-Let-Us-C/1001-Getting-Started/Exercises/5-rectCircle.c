/*
 * @Author: vgopi1 
 * @Date: 2020-08-09 01:07:09 
 * @Last Modified by: vgopi1
 * @Last Modified time: 2020-08-09 01:19:32
 */
/* 
The length and breadth of a rectangle and radius of a circle are
input through the keyboard. Write a program to calculate the area
and perimeter of the rectangle, and the area and circumference of
the circle.
*/
#include <stdio.h>
#define PI 3.141592653589793238462643383279

int main(void)
{
    float length = 0, breadth = 0, radius = 0;

    printf("Enter the length, breadth, and width: \n");
    scanf("%f %f %f", &length, &breadth, &radius);

    printf("The area of the rectangle is: %f\n", length*breadth); 
    printf("The perimeter of the rectangle is: %f\n", length*breadth);
    printf("The area of the circle is: %f\n", PI*radius*radius);

    return 0;
}