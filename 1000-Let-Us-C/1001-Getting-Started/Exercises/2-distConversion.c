/*
 * @Author: vgopi1 
 * @Date: 2020-08-09 00:32:28 
 * @Last Modified by: vgopi1
 * @Last Modified time: 2020-08-09 00:43:25
 * @Prompt: The distance between two cities (in km.) is input through the keyboard. Convert and print this distance in
meters, feet, inches and centimeters.
 */

#include <stdio.h>

int main(void)
{
    float dist = 0; 

    printf("Please enter the distance between two cities in km: \n");
    scanf("%f", &dist);
    printf("The distance between two cities in meters is %.2f\n", dist*1000);
    printf("The distance between two cities in centimeters is %.2f\n", dist*1000*100);
    printf("The distance between two cities in inches is %.2f\n", dist*1000*100/2.54);
    printf("The distance between two cities in feet is %.2f\n", (dist*1000*100/2.54)/12);

    return 0;
}
