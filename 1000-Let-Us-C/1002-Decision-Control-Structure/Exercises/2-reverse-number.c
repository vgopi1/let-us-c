/*
 * @Author: vgopi1 
 * @Date: 2020-08-14 22:01:41 
 * @Last Modified by: vgopi1
 * @Last Modified time: 2020-08-14 22:10:32
 */

#include <stdio.h>

int main()
{
    int num = 0; int digit1 = 0; int digit2 = 0; int digit3 = 0; int digit4 = 0; int digit5 = 0;
    printf("Please enter your five-digit number");
    scanf("%d", &num);

    digit5 = num % 10;
    digit4 = num/10 % 10;
    digit3 = num/100 % 10;
    digit2 = num/1000 % 10;
    digit1 = num/10000 % 10;
    printf("The reversed number is: %d%d%d%d%d", digit5,digit4,digit3,digit2,digit1);

    return 0;
}