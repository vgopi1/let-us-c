/*
 * @Author: vgopi1 
 * @Date: 2020-08-19 22:22:56 
 * @Last Modified by: vgopi1
 * @Last Modified time: 2020-08-19 22:48:51
 * Write a program to receive values of latitude (L1, L2) and longitude
(G1, G2), in degrees, of two places on the earth and output the
distance (D) between them in nautical miles. The formula for
distance in nautical miles is:
D = 3963 cos
-1
( sin L1 sin L2 + cos L1 cos L2 * cos ( G2 – G1 ) ) 
 */
#include <stdio.h>
#include <math.h>

int main()
{
    float l1 = 0, l2 = 0, g1 = 0, g2 = 0;
    scanf("%f %f %f %f", l1, g1, l2, g2);
    printf("The distance in nautical miles is %f", 3963*arccos(sin(l1)*sin(l2) + cos(l1)*cos(l2) * cos(g2-g2)));
    return 0;
}
