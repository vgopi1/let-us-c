/*
 * @Author: vgopi1 
 * @Date: 2020-08-14 22:21:50 
 * @Last Modified by: vgopi1
 * @Last Modified time: 2020-08-14 22:23:35
 */
#include <stdio.h>
#include <math.h>

int main()
{
    float x = 0, y = 0;
    printf("Enter the Cartesian Coordinates:");
    scanf("%f %f", &x, &y); 
    float r = sqrt(x*x + y*y);
    float theta = atan(y/x);
    printf("The polar coordinate is (%.2f, %.2f)", r, theta );
    return 0;
}