/*
 * @Author: vgopi1 
 * @Date: 2020-08-27 10:42:59 
 * @Last Modified by:   vgopi1 
 * @Last Modified time: 2020-08-27 10:42:59 
 * FIRST PROGRAM FOR TODAY
 * Consider a currency system in which there are notes of seven
denominations, namely, Re. 1, Rs. 2, Rs. 5, Rs. 10, Rs. 50, Rs. 100. If
a sum of Rs. N is entered through the keyboard, write a program to
compute the smallest number of notes that will combine to give Rs.
N.
 */
#include <stdio.h>
#include <math.h>
int main()
{
    int num = 1101, count = 0;
    //printf("Please enter how many rupees you have in your life savings: ");
    //scanf("%d", &num);
  
        count += num / 100;
        num -= num / 100 * 100; 
     
        count += num / 50;
        num -= num / 50 * 50; 
    
        count += num / 10;
        num -= num / 10 * 10; 
     
        count += num / 5;
        num -= num / 5 * 5; 
    
        count += num / 2;
        num -= num/ 2 * 2; 

        count += num / 1;

    printf("%d", count);
    return 0;
}
