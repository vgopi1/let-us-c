/*
 * @Author: vgopi1 
 * @Date: 2020-08-09 22:00:13 
 * @Last Modified by: vgopi1
 * @Last Modified time: 2020-08-14 22:01:44
 * Prompt: If a five-digit number is input through the keyboard, write a
program to calculate the sum of its digits. (Hint: Use the modulus
operator ‘%’)
 */

#include <stdio.h>

int main()
{
    int num = 0; int digit1 = 0; int digit2 = 0; int digit3 = 0; int digit4 = 0; int digit5 = 0;
    printf("Please enter your five-digit number");
    scanf("%d", &num);

    digit5 = num % 10;
    digit4 = num/10 % 10;
    digit3 = num/100 % 10;
    digit2 = num/1000 % 10;
    digit1 = num/10000 % 10;
    printf("The sum of the digits is: %d", digit1+digit2+digit3+digit4+digit5);

    return 0;
}