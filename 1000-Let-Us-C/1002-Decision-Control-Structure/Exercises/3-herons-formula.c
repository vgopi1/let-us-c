/*
 * @Author: vgopi1 
 * @Date: 2020-08-14 22:12:03 
 * @Last Modified by: vgopi1
 * @Last Modified time: 2020-08-14 22:15:50
 * prompt: Given three sides calculate the area of the triangle.
 */

#include <stdio.h>
#include <math.h>

int main()
{
    float side1 = 0, side2 = 0, side3 = 0;
    printf("Enter the sides of the triangle:");
    scanf("%f %f %f", &side1, &side2, &side3);
    float semiperimeter = (side1+side2+side3)/2;
    float area = sqrt(semiperimeter*(semiperimeter-side1)*(semiperimeter-side2)*(semiperimeter-side3));
    printf("The area of the triangle is: %f\n",  area);
}
