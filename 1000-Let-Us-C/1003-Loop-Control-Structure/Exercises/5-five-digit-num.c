/*
 * @Author: vgopi1 
 * @Date: 2020-08-28 18:07:30 
 * @Last Modified by: vgopi1
 * @Last Modified time: 2020-08-28 18:14:02
 * @Prompt: reverse five digit num and check if it equals the orig
 */

#include <stdio.h>

int main()
{
    int num = 0, d1 = 0, d2 = 0, d3 = 0, d4 = 0, d5 = 0;
    printf("Enter a number: ");
    scanf("%d", &num);

    d1 = num % 10; // 987 % 10 = 7
    d2 = num/10 % 10; // 987/10 = 98 % 10 = 8
    d3 = num/100 % 10; // 987 / 100 = 9 % 10 = 9
    d4 = num/1000 % 10;
    d5 = num/10000 % 10;

    int newNum = d5 + d4*10 + d3*100 + d2*1000 + d1*10000;

    printf("%d\n", newNum);
    if(newNum == num)
        printf("The reversed number equals the original.\n");

    return 0;
}