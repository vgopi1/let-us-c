/*
 * @Author: vgopi1 
 * @Date: 2020-08-27 11:37:45 
 * @Last Modified by: vgopi1
 * @Last Modified time: 2020-08-28 18:07:04
 */

#include <stdio.h>

int main()
{
    int num = 0;
    printf("Please enter the number: ");
    scanf("%d", &num);

    if (num % 2 == 0)
        printf("\nYour number is even.\n");
    else
    {
        printf("\nYour number is odd\n");
    }
    return 0;
}