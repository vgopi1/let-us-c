/*
 * @Author: vgopi1 
 * @Date: 2020-08-28 17:07:02 
 * @Last Modified by: vgopi1
 * @Last Modified time: 2020-08-28 17:12:14
 *Any year is input through the keyboard. Write a program to
determine whether the year is a leap year or not.
 */
#include <stdio.h>

int main()
{
    int year = 0;
    printf("Please enter a year and I'll tell you if it's a leap year:");
    scanf("%d", &year);
    if (year % 4 == 0)
        printf("The year is a leap year.");
    else
        printf("The year is not a leap year.");
    return 0;
}