// Monday on 1/01/2001  
// What day is 1/1/2001 this year

#include <stdio.h>

int main()
{
    int year = 0;
    printf("Enter a year:");
    scanf("%d", &year);
    if (year % 7 == 1)
        printf("Day is Monday");
    if (year % 7 == 2)
    printf("Day is Tuesday");
    if (year % 7 == 3)
    printf("Wednesday");
    if (year % 7 == 4)
    printf("Thursday");
    if (year % 7 == 5)
    printf("Friday");
    if (year % 7 == 6)
    printf("Saturday");
    if (year % 7 == 0)
    printf("Sunday");
    return 0;
}