/*
 * @Author: vgopi1 
 * @Date: 2020-08-28 18:14:40 
 * @Last Modified by: vgopi1
 * @Last Modified time: 2020-08-28 18:18:03
 * @Prompt: Ram, Shyam, and Ajay input their ages. Who's the youngest?
 */
#include <stdio.h>

int main()
{
    int ram, shyam, ajay = 0; //@var for ages
    printf("Enter ages of Ram, Shyam, and Ajay respectively: \n");
    scanf("%d %d %d", &ram, &shyam, &ajay);

    if (ram < shyam && ram < ajay)
        printf("Ram is the youngest.");
    if (shyam < ram && shyam < ajay)
        printf("Shyam is the youngest.");
    if (ajay < ram && ajay < shyam)
        printf("Ajay is the youngest.");
    else 
        printf("Nobody is the youngest!");



    return 0;
}