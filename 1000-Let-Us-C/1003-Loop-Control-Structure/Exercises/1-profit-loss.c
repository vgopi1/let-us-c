/*
 * @Author: vgopi1 
 * @Date: 2020-08-27 10:44:01 
 * @Last Modified by: vgopi1
 * @Last Modified time: 2020-08-27 11:37:28
 If cost price and selling price of an item are input through the
keyboard, write a program to determine whether the seller has
made profit or incurred loss. Also determine how much profit he
made or loss he incurred.
 */

#include <stdio.h>

int main()
{
    float cp = 0, sp = 0; //@var cp = cost price, @var sp = selling price
    printf("Please enter the cost price and selling price of the item: ");
    scanf("%f %f", &cp, &sp);

    if (cp > sp)
        printf("The loss is $%.2f", cp - sp);
    if (sp > cp)
        printf("The profit is $%.2f", sp - cp);
    
    printf("\nThank you for banking with us!\n");
    
    return 0;
}