# AMMA'S LIST OF GOOD CODING PRACTICES
1. **Naming Convention** - Descriptive and clear variable/function/methods/objects names; Verb-Noun Naming (e.g calSum, isNumeric); Use CamelCase or Underscores
2. **DRY** - Don't Repeat Yourself - Use preprocessor, functions, etc.
3. **Scalable**- Be able to accept any number of arguments, think ahead rgd different usecases for your program
4. **Modular** - Be able to break it down into individual lego blocks (like functions) each with its own separate purpose/utility.
5. **Comments** - Be able to replace documentation/tutorials - e.g for Functions - Purpose, Parameters, Variables; 
On top, you can have the Package/File Name, Author/Contact, Date, Purpose, License, etc.
6. **Error Handling**
7. **Input Validation/ Input Elimination**
8. **Exception Handling**
9. **LOGGING** - Log errors for the purpose of fixing them.
10. **Configuration files (settings, username/password)** - backup the original file before editing- Always keep a copy that works / copy of folder that works.
